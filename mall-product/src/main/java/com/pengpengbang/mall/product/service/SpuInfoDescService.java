package com.pengpengbang.mall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pengpengbang.common.utils.PageUtils;
import com.pengpengbang.mall.product.entity.SpuInfoDescEntity;

import java.util.Map;

/**
 * spu信息介绍
 *
 * @author pengziyi
 * @email 870186504@qq.com
 * @date 2020-09-11 02:01:26
 */
public interface SpuInfoDescService extends IService<SpuInfoDescEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

