package com.pengpengbang.mall.product.dao;

import com.pengpengbang.mall.product.entity.AttrEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品属性
 * 
 * @author pengziyi
 * @email 870186504@qq.com
 * @date 2020-09-11 02:01:26
 */
@Mapper
public interface AttrDao extends BaseMapper<AttrEntity> {
	
}
