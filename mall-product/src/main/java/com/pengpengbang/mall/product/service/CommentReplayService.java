package com.pengpengbang.mall.product.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.pengpengbang.common.utils.PageUtils;
import com.pengpengbang.mall.product.entity.CommentReplayEntity;

import java.util.Map;

/**
 * 商品评价回复关系
 *
 * @author pengziyi
 * @email 870186504@qq.com
 * @date 2020-09-11 02:01:26
 */
public interface CommentReplayService extends IService<CommentReplayEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

