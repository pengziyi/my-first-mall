package com.pengpengbang.mall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pengpengbang.common.utils.PageUtils;
import com.pengpengbang.mall.order.entity.OrderEntity;

import java.util.Map;

/**
 * 订单
 *
 * @author pengziyi
 * @email 870186504@qq.com
 * @date 2020-09-12 11:47:56
 */
public interface OrderService extends IService<OrderEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

