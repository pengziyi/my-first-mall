package com.pengpengbang.mall.order.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pengpengbang.common.utils.PageUtils;
import com.pengpengbang.mall.order.entity.OrderItemEntity;

import java.util.Map;

/**
 * 订单项信息
 *
 * @author pengziyi
 * @email 870186504@qq.com
 * @date 2020-09-12 11:47:56
 */
public interface OrderItemService extends IService<OrderItemEntity> {
    PageUtils queryPage(Map<String, Object> params);

    //add new feature
    PageUtils queryPage2(Map<String, Object> params);

    //this is hotfix
    PageUtils hotfix(Map<String, Object> params);
}

