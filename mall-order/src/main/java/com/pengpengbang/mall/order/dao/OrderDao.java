package com.pengpengbang.mall.order.dao;

import com.pengpengbang.mall.order.entity.OrderEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 订单
 * 
 * @author pengziyi
 * @email 870186504@qq.com
 * @date 2020-09-12 11:47:56
 */
@Mapper
public interface OrderDao extends BaseMapper<OrderEntity> {
	
}
