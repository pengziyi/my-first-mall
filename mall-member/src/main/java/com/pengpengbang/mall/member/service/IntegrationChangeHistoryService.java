package com.pengpengbang.mall.member.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pengpengbang.common.utils.PageUtils;
import com.pengpengbang.mall.member.entity.IntegrationChangeHistoryEntity;

import java.util.Map;

/**
 * 积分变化历史记录
 *
 * @author pengziyi
 * @email 870186504@qq.com
 * @date 2020-09-12 11:44:07
 */
public interface IntegrationChangeHistoryService extends IService<IntegrationChangeHistoryEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

