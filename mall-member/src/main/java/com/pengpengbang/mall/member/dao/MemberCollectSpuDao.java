package com.pengpengbang.mall.member.dao;

import com.pengpengbang.mall.member.entity.MemberCollectSpuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 会员收藏的商品
 * 
 * @author pengziyi
 * @email 870186504@qq.com
 * @date 2020-09-12 11:44:07
 */
@Mapper
public interface MemberCollectSpuDao extends BaseMapper<MemberCollectSpuEntity> {
	
}
