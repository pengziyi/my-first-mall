package com.pengpengbang.mall.ware.dao;

import com.pengpengbang.mall.ware.entity.WareSkuEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 商品库存
 * 
 * @author pengziyi
 * @email 870186504@qq.com
 * @date 2020-09-12 11:50:00
 */
@Mapper
public interface WareSkuDao extends BaseMapper<WareSkuEntity> {
	
}
