package com.pengpengbang.mall.ware.dao;

import com.pengpengbang.mall.ware.entity.WareInfoEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 仓库信息
 * 
 * @author pengziyi
 * @email 870186504@qq.com
 * @date 2020-09-12 11:50:00
 */
@Mapper
public interface WareInfoDao extends BaseMapper<WareInfoEntity> {
	
}
