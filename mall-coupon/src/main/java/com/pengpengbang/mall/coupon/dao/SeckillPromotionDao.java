package com.pengpengbang.mall.coupon.dao;

import com.pengpengbang.mall.coupon.entity.SeckillPromotionEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * 秒杀活动
 * 
 * @author pengziyi
 * @email 870186504@qq.com
 * @date 2020-09-12 11:05:30
 */
@Mapper
public interface SeckillPromotionDao extends BaseMapper<SeckillPromotionEntity> {
	
}
