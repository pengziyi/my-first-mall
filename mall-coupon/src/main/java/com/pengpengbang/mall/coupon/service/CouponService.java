package com.pengpengbang.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pengpengbang.common.utils.PageUtils;
import com.pengpengbang.mall.coupon.entity.CouponEntity;

import java.util.Map;

/**
 * 优惠券信息
 *
 * @author pengziyi
 * @email 870186504@qq.com
 * @date 2020-09-12 11:05:30
 */
public interface CouponService extends IService<CouponEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

