package com.pengpengbang.mall.coupon.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pengpengbang.common.utils.PageUtils;
import com.pengpengbang.mall.coupon.entity.SeckillSessionEntity;

import java.util.Map;

/**
 * 秒杀活动场次
 *
 * @author pengziyi
 * @email 870186504@qq.com
 * @date 2020-09-12 11:05:30
 */
public interface SeckillSessionService extends IService<SeckillSessionEntity> {

    PageUtils queryPage(Map<String, Object> params);
}

